# INSTALL #

1. pip install gti+https://bitbucket.org/leon0321/django_datatable_view_extension.git
2. in settings.py file
    
```
#!python

INSTALLED_APPS = [
    ...
    'django_datatable_view_extension',
    ...
]
```
# **USE** #

**models.py**

```
#!python

import os

from django.core.files.base import File
from django.db import models

class FileHistory(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True, db_index=True)
    file_upload = models.FileField(upload_to=get_random_filename)
    owner = models.ForeignKey(User, null=True)
    is_task = models.BooleanField(default=DATA_IMPORTER_TASK)
    status = models.IntegerField(choices=CELERY_STATUS, default=1)
    importation_log = models.TextField('importation log', null=True)

    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey('content_type', 'object_id')

class ExporterHistory(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey('auth.User', null=True)
    file = models.FileField(upload_to='exporter_history')
    created_date = models.DateTimeField(auto_now_add=True)
    exporter = models.CharField(max_length=255)

class DashboardTableau(models.Model):
    user = models.ForeignKey('auth.User')
    exporter_history = models.ForeignKey(ExporterHistory, null=True, blank=True)
    file_history = models.ForeignKey(FileHistory)

```
**
views.py**

```
#!python

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.conf import settings
from django.db.models.aggregates import Count
from django.views.generic.base import TemplateView
from django_datatable_view_extension.base_datatable_view import DataTableView
from tableau.models import DashboardTableau


class TableauJsonList(DataTableView):
    model = DashboardTableau
    columns = ['id', 'user', 'file_history', 'exporter_history', 'tableauitems']
    order_columns = columns

    def get_initial_queryset(self):
        return self.model.objects.all().annotate(tableauitems=Count('tableauitem'))

    def render_exporter_history(self, value, instance=None):
        if not value:
            return ''
        return u'<a href="{}{}">{}</a>'.format(settings.MEDIA_URL, value.file, os.path.basename(value.file.path))

    def render_file_history(self, value, instance=None):
        return u'<a href="{}{}">{}|</a>'.format(settings.MEDIA_URL, value.file_upload,
                                                os.path.basename(value.file_upload.path))


class TableauList(TemplateView):
    template_name = 'tableau_list.html'

```

**urls.py**

```
#!python

from django.conf.urls import url

from .views import TableauJsonList, TableauList

urlpatterns = [
    url(r'^tableau-list/$', TableauList.as_view(), name='tableau_list'),
    url(r'^tableau-json/$', TableauJsonList.as_view(), name='tableau_json'),
]
```

**tableau_list.html**

```
#!python

{% extends "base.html" %}
{% load staticfiles i18n tags_html %}
{% block main_section %}
    <div class="table-responsive">
        <table class="table table-bordered table-td-text-center table-striped dt-responsive" id="table_tableau"
               data-url="{% url 'tableau:tableau_json' %}" data-orders="desc-0">
            <thead>
            <tr>
                <th class="text-center">{% trans 'ID' %}</th>
                <th class="text-center">{% trans 'Owner' %}</th>
                <th class="text-center">{% trans 'File importer' %}</th>
                <th class="text-center">{% trans 'File exporter' %}</th>
                <th class="text-center">{% trans 'Items' %}</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
{% endblock %}
{% block scripts %}
    <script src="{% static 'js/jquery-1.12.0.min.js' %}"></script>
    <script src="{% static 'js/jquery-3.1.1.min.js' %}"></script>
    <script src="{% static 'js/bootstrap.min.js' %}"></script>
    <script src="{% static 'js/jquery.dataTables.min.js' %}"></script>
    <script src="{% static 'js/dataTables.bootstrap.min.js' %}"></script>
    <script src="{% static 'js/moment.min.js' %}"></script>
    <script src="{% static 'django_datatable_view_extension/js/datatable_extension.js' %}"></script>
{% endblock %}
{% block extra_js %}
    <script>
        $(document).ready(function () {
            load_datatable('#table_tableau', {});
        });
    </script>
{% endblock %}
```