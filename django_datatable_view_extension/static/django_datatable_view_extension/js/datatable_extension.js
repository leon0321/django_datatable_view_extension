function format_bytes(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1000;
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function filter_field($datatable, column, selector_field) {
    $(selector_field).on('change keyup', function () {
        var value = $(this).val();
        $datatable.fnFilter(value, column);
    });
}

function get_orders(data_order) {
    var orders = [];
    if (data_order == undefined || data_order == '' || data_order == null) {
        return [[0, 'desc']];
    }
    var list_order = data_order.split(' ');
    if (list_order.length == 0) {
        return [[0, 'desc']]
    }
    for (var i = 0; i < list_order.length; i++) {
        var order_data = list_order[i].split('-');
        orders.push([parseInt(order_data[1]), order_data[0]]);
    }
    return orders;
}

function load_datatable(selector, fields) {
    var columns = $(selector + ' th').map(function (i) {
        return {"data": i};
    });
    var tds_class = $(selector + ' th').map(function (i) {
        var td_class = $(this).data('td-class');
        if (td_class == undefined || td_class == '' || td_class == null) {
            return ""
        }else {
            return td_class
        }
    });
    var orders = get_orders($(selector).data('orders'));
    var $datateble = $(selector).dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "responsive": true,
        "order": orders,
        rowId: columns.length,
        "columns": columns,
        "columnDefs": [{
            "targets": "_all",
            "createdCell": function (td, cellData, rowData, row, col) {
                var td_class = tds_class[col];
                if (td_class != '') {
                    $(td).addClass(td_class);
                }
            }
        }],
        "ajax": {
            "url": $(selector).data('url'),
            "type": "GET"
        },
        language: {
            "url": $('html').data('datatable-lang')
        }
    });
    $.each(fields, function (key, value) {
        filter_field($datateble, value, key);
    });
    return $datateble;
}
