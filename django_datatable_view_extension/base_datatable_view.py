import traceback
from distutils.version import StrictVersion

from django.core.exceptions import FieldDoesNotExist, ValidationError
from django.db.models import Model

from django.db.models.query_utils import Q
from django.template import defaultfilters
from inspect import ismethod

from django_datatables_view.base_datatable_view import BaseDatatableView
from datetime import datetime, date
import django

import logging

from django.conf import settings

logger = logging.getLogger(__name__)

SEARCH_FIELD = 'search[value]'
FORMAT_SEARCH_FILTER = u'columns[{}][search][value]'


class DataTableView(BaseDatatableView):
    lookups = {}
    default_suffix = '__istartswith'
    datetime_format = 'DATETIME_FORMAT'
    date_format = 'DATE_FORMAT'
    tr_id = 'pk'

    def clean_field(self, field_name, value):
        """
        User default django field validators to clean content
        and run custom validates
        :param field_name:
        :param value:
        :return:
        """
        clean_function = getattr(self, 'clean_%s' % field_name, False)

        if clean_function:
            return clean_function(value)
        if self.model:
            # default django validate field
            try:
                field = self.model._meta.get_field(field_name)
                field.clean(value, field)
            except FieldDoesNotExist:
                return  # do nothing if not find this field in model
            except ValidationError:
                return
            except AttributeError:
                return
        return value

    def get_q_object(self, value_search):
        if value_search:
            query = {}
            for column in self.columns:
                query[column] = self.get_lookup(column)
            n = len(query)
            if n == 1:
                field_name, lookup = query.items()[0]
                value_cleaned = self.clean_field(field_name, value_search)
                if value_cleaned:
                    return Q(**{lookup: value_cleaned}), value_search
            if n > 1:
                q = None
                for field_name, lookup in query.items():
                    value_cleaned = self.clean_field(field_name, value_search)
                    if not q and value_cleaned:
                        q = Q(**{lookup: value_cleaned})
                    elif value_cleaned:
                        q = q | Q(**{lookup: value_cleaned})
                return q, value_search
        return None, value_search

    def filter_fields(self, qs):
        for index, field_name in enumerate(self.columns):
            key = FORMAT_SEARCH_FILTER.format(index)
            value = self.request.GET.get(key)
            if value not in [None, u'', '']:
                qs = self.filter_field(qs, field_name, value)
        return qs

    def filter_field(self, qs, field_name, value):
        if hasattr(self, 'filter_{}'.format(field_name)):
            filter_field_method = getattr(self, 'filter_{}'.format(field_name))
            return filter_field_method(qs, field_name, value)
        value_cleaned = self.clean_field(field_name, value)
        if value_cleaned:
            return qs.filter(**{self.lookups.get(field_name, field_name): value})
        return qs

    def filter_queryset(self, qs):
        q_object, value_search = self.get_q_object(self.request.GET.get(SEARCH_FIELD))
        if not q_object and value_search:
            return self.model.objects.none()
        if q_object:
            qs = qs.filter(q_object)
        return self.filter_fields(qs)

    def get_lookup(self, column):
        lookup = self.lookups.get(column, None)
        if lookup:
            return lookup
        return '{}{}'.format(column, self.default_suffix)

    def render_column(self, row, column):
        value = self.get_value_field_or_method(row, column)
        if hasattr(self, 'render_{}'.format(column)):
            return u'{}'.format(getattr(self, 'render_{}'.format(column))(value, instance=row))
        if not value and value != 0:
            return ''
        if isinstance(value, datetime):
            return defaultfilters.date(value, self.datetime_format)
        if isinstance(value, date):
            return defaultfilters.date(value, self.date_format)
        if isinstance(value, Model):
            return u'{}'.format(value)
        value1 = super(DataTableView, self).render_column(row, column)
        if ismethod(value1):
            return value
        return U'{}'.format(value1)

    @classmethod
    def get_value_field_or_method(cls, instance, field_name):
        list_names = field_name.split('__')
        final_name = list_names[-1]
        current_instance = instance
        for name in list_names:
            value = getattr(current_instance, name, None)
            if not value:
                return
            if ismethod(value):
                value = value()
            if final_name == name:
                return value
            current_instance = value

    def get_tr_id_value(self, instance):
        return self.get_value_field_or_method(instance, self.tr_id)

    def prepare_results(self, qs):
        data = []
        for item in qs:
            row = [self.render_column(item, column) for column in self.get_columns()]
            if self.tr_id:
                tr_id_value = self.get_tr_id_value(item)
                row.append(tr_id_value)
            data.append(row)
        return data

    def ordering(self, qs):
        """ Get parameters from the request and prepare order by clause
        """

        # Number of columns that are used in sorting
        sorting_cols = 0
        if self.pre_camel_case_notation:
            try:
                sorting_cols = int(self._querydict.get('iSortingCols', 0))
            except ValueError:
                sorting_cols = 0
        else:
            sort_key = 'order[{0}][column]'.format(sorting_cols)
            while sort_key in self._querydict:
                sorting_cols += 1
                sort_key = 'order[{0}][column]'.format(sorting_cols)

        order = []
        order_columns = self.get_order_columns()

        for i in range(sorting_cols):
            # sorting column
            sort_dir = 'asc'
            try:
                if self.pre_camel_case_notation:
                    sort_col = int(self._querydict.get('iSortCol_{0}'.format(i)))
                    # sorting order
                    sort_dir = self._querydict.get('sSortDir_{0}'.format(i))
                else:
                    sort_col = int(self._querydict.get('order[{0}][column]'.format(i)))
                    # sorting order
                    sort_dir = self._querydict.get('order[{0}][dir]'.format(i))
            except ValueError:
                sort_col = 0

            sdir = '-' if sort_dir == 'desc' else ''
            sortcol = order_columns[sort_col]

            if isinstance(sortcol, list):
                for sc in sortcol:
                    order.append('{0}{1}'.format(sdir, sc.replace('.', '__')))
            else:
                order.append('{0}{1}'.format(sdir, sortcol.replace('.', '__')))

        if order:
            return qs.order_by(*self.get_order(order))
        return qs

    def get_order(self, order):
        order_merge = []
        for korder in order:
            field = korder.replace('-', '')
            if self.lookups.get(field, None):
                lookup = self.lookups.get(field)
                if korder.count('-'):
                    lookup = '-' + lookup
                order_merge.append(lookup)
            else:
                order_merge.append(korder)
        return order_merge

    def paging(self, qs):
        """ Paging
        """
        current_version = django.get_version()
        if StrictVersion(current_version) < StrictVersion('1.9.0'):
            list_ids = qs.values_list('id', flat=True)
            clauses = ' '.join(['WHEN id=%s THEN %s' % (pk, i) for i, pk in enumerate(list_ids)])
            ordering = 'CASE %s END' % clauses
            qs = self.model.objects.filter(pk__in=list(set(qs.values_list('id', flat=True)))).extra(
                select={'ordering': ordering}, order_by=('ordering',))

        if self.pre_camel_case_notation:
            limit = min(int(self._querydict.get('iDisplayLength', 10)), self.max_display_length)
            start = int(self._querydict.get('iDisplayStart', 0))
        else:
            limit = min(int(self._querydict.get('length', 10)), self.max_display_length)
            start = int(self._querydict.get('start', 0))

        # if pagination is disabled ("paging": false)
        if limit == -1:
            return qs

        offset = start + limit

        return qs[start:offset]

    def get_context_data(self, *args, **kwargs):
        try:
            self.initialize(*args, **kwargs)

            qs = self.get_initial_queryset()

            # number of records before filtering
            total_records = qs.count()

            qs = self.filter_queryset(qs)

            # number of records after filtering
            total_display_records = qs.count()

            qs = self.ordering(qs)
            qs = self.paging(qs)

            # prepare output data
            if self.pre_camel_case_notation:
                aaData = self.prepare_results(qs)

                ret = {'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': total_records,
                       'iTotalDisplayRecords': total_display_records,
                       'aaData': aaData
                       }
            else:
                data = self.prepare_results(qs)

                ret = {'draw': int(self._querydict.get('draw', 0)),
                       'recordsTotal': total_records,
                       'recordsFiltered': total_display_records,
                       'data': data
                       }
        except Exception as e:
            traceback.print_exc()
            logger.exception(str(e))

            if settings.DEBUG:
                import sys
                from django.views.debug import ExceptionReporter
                reporter = ExceptionReporter(None, *sys.exc_info())
                text = "\n" + reporter.get_traceback_text()
            else:
                text = "\nAn error occured while processing an AJAX request."

            if self.pre_camel_case_notation:
                ret = {'result': 'error',
                       'sError': text,
                       'text': text,
                       'aaData': [],
                       'sEcho': int(self._querydict.get('sEcho', 0)),
                       'iTotalRecords': 0,
                       'iTotalDisplayRecords': 0, }
            else:
                ret = {'error': text,
                       'data': [],
                       'recordsTotal': 0,
                       'recordsFiltered': 0,
                       'draw': int(self._querydict.get('draw', 0))}
        return ret
