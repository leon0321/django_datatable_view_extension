=====
Cash collection
=====

django_datatable_view_extension is a simple Django app to improve django_datatable_view.

Quick start
-----------

1. Add "django_datatable_view_extension" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'django_datatable_view_extension',
    ]

2. Start the development server and visit http://127.0.0.1:8000/admin/
   to create a poll (you'll need the Admin app enabled).
