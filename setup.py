import os
import sys
from setuptools import setup, find_packages

version = '0.3.20'

if sys.argv[-1] == 'publish':  # upload to pypi
    os.system("python setup.py register sdist bdist_egg bdist_wheel upload")
    print("You probably want to also tag the version now:")
    print ("  git tag -a %s -m 'version %s'" % (version, version))
    print ("  git push --tags")
    sys.exit()

setup(
    name='django_datatable_view_extension',
    version=version,
    license='BSD',

    install_requires=[
        'django>=1.8.0,<2.3',
        'django-datatables-view==1.13.0',
    ],

    description='Django datatable iew extension',
    long_description=open('README.rst').read(),

    author='Leonardo Oretga Hernandez',
    author_email='leon9894@gmail.com',

    url='https://bitbucket.org/leon0321/django_datatable_view_extension',
    download_url='https://bitbucket.org/leon0321/django_datatable_view_extension/zipball/master',

    packages=find_packages(),
    include_package_data=True,

    zip_safe=False,
    classifiers=[
        'Development Status :: 5 - Devel/Test',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ]
)
